//=====================================================
//  Connect Four by Alec Stephens and Patterson Day
//=====================================================

//=====================================================
//    Global Variables and  Gameboard Generation
//=====================================================



let gameActive = true;
let activePlayer = 1;
let gameBoard = [];
let playerColor = [];
playerColor[1] = "red"
playerColor[2] = "black"
let gameboardTarget = document.querySelector('main')

const gameboard = [
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0]
];

for (let columnIndex = 0; columnIndex < gameboard.length; columnIndex++) {
    const columnModel = gameboard[columnIndex];
    const columnElement = document.createElement('div');
    columnElement.classList.add('column');
    columnElement.id = "column-" + columnIndex
    columnElement.dataset.columnIndex = columnIndex;
    gameboardTarget.appendChild(columnElement);


    for (let cellIndex = 0; cellIndex < columnModel.length; cellIndex++) {
        const cell = columnModel[cellIndex];
        const cellElement = document.createElement('div');
        cellElement.id = columnIndex + "-" + cellIndex;
        cellElement.classList.add('cell');
        cellElement.dataset.columnIndex = columnIndex;
        cellElement.dataset.cellIndex = cellIndex;
        columnElement.appendChild(cellElement);
    }
}

whichTurn()
//=========================================
//              Functions
//=========================================

//This function determines who's turn it is

function whichTurn() {

    if (gameActive) {
        document.getElementById('game-info').innerHTML = "Current Player: Player " + activePlayer + " <span class = 'player" + activePlayer + "'>(" + playerColor[activePlayer] + ") </span>";
    }
}
//This function replaces the space value from 0 to either 1 or 2 depending on the active player

function placeChip(event) {
    let clickedCol = event.currentTarget;
    let clickedIndexCol = clickedCol.id.replace("column-", "")
    if (gameActive) {
        for (let index = clickedCol.childElementCount - 1; index >= 0; index--) {
            if (gameboard[clickedIndexCol][index] == 0) {
                const cellElement = document.getElementById(clickedIndexCol + "-" + index)
                gameboard[clickedIndexCol][index] = activePlayer;
                cellElement.style.backgroundColor = playerColor[activePlayer];
                cellElement.style.borderRadius = "50%"
                if (activePlayer == 1) {
                    activePlayer = 2;
                } else {
                    activePlayer = 1;
                }
                break;
            }
        }
    }
    endGame();
    console.log(gameboard);
}

//Vertical Win
function checkVerticalWin() {
    for (let player = 1; player <= 2; player++) {
        for (let row = 0; row <= 3; row++) {
            for (let col = 0; col <= 6; col++) {
                if (gameboard[col][row] == player) {
                    if ((gameboard[col][row + 1] == player) && (gameboard[col][row + 2] == player) && (gameboard[col][row + 3] == player)) {
                        return true
                    }

                }
            }
        }
    }
}
//Horizontal Win
function checkHorizontalWin() {
    for (let player = 1; player <= 2; player++) {
        for (let row = 0; row <= 6; row++) {
            for (let col = 0; col <= 3; col++) {
                if (gameboard[col][row] == player) {
                    if ((gameboard[col + 1][row] == player) && (gameboard[col + 2][row] == player) && (gameboard[col + 3][row] == player)) {
                        return true
                    }

                }
            }
        }
    }
}

//Diagonal Down Win
function checkDiagonalDownWin() {
    for (let player = 1; player <= 2; player++) {
        for (let row = 0; row <= 3; row++) {
            for (let col = 2; col <= 6; col++) {
                if (gameboard[col][row] == player) {
                    if ((gameboard[col - 1][row + 1] == player) && (gameboard[col - 2][row + 2] == player) && (gameboard[col - 3][row + 3] == player)) {
                        return true
                    }

                }
            }
        }
    }
}
//Diagonal Up Win
function checkDiagonalUpWin() {
    for (let player = 1; player <= 2; player++) {
        for (let row = 0; row <= 6; row++) {
            for (let col = 0; col <= 3; col++) {
                if (gameboard[col][row] == player) {
                    if ((gameboard[col + 1][row + 1] == player) && (gameboard[col + 2][row + 2] == player) && (gameboard[col + 3][row + 3] == player)) {
                        return true
                    }

                }
            }
        }
    }
}

//Checks for tie game and returns boolean

function checkTie() {
    if (arraySum(gameboard) > 62) {
        return true;
    }
}

//Produces win message once conditions are met

function endGame(winningPlayer) {
    if ((checkVerticalWin()) || (checkHorizontalWin()) || (checkDiagonalDownWin()) || (checkDiagonalUpWin())) {
        gameActive = false;
        if (activePlayer == 1) { winningPlayer = 2 }
        else { winningPlayer = 1 }
        console.log("Game is won!");
        console.log(winningPlayer);
        document.getElementById("game-winner").innerHTML = "Player " + winningPlayer + " has won the game!";
    }
    else if ((checkTie())) {
        console.log("It's a tie!");
        document.getElementById("game-winner").innerHTML = "It's a tie game!";
    }
}

//==============================================================================
//Found function to add sum of nested array from Tibos on StackOverflow
//https://stackoverflow.com/questions/19181515/finding-the-sum-of-a-nested-array
//https://jsbin.com/eGaFOLA/2/edit?html,js,console
//==============================================================================
function arraySum(i) {
    let sum = 0;
    for (let a = 0; a < i.length; a++) {
        if (typeof i[a] == "number") {
            sum += i[a];
        } else if (i[a] instanceof Array) {
            sum += arraySum(i[a]);
        }
    }
    return sum;
}


//==============================
//      Click Handlers
//==============================

const columns = document.getElementsByClassName("column")
for (let column of columns) {
    column.addEventListener('click', (event) => {
        placeChip(event)
        whichTurn()
    })
}